﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace DfRawEditor
{
    public partial class dfRawEditor : Form
    {
        private XmlDocument xmlDataDocument;
        private XmlDocument xmlResultDocument;
        private TreeNode selectedNode;
        private XmlNode selectedXmlNode;
        private TreeNodeCollection elementTreeNodes;
        public static List<String> fileNamesList;
        // TMP Var
        public static String myErrorString;

        public dfRawEditor()
        {
            InitializeComponent();
        }

        private void btnChangeFolder_Click(object sender, EventArgs e)
        {
            //DialogResult result = this.fbDialog.ShowDialog();
            //if (result == DialogResult.OK)
            {
                this.tsPanel.Enabled = true;
                this.lFileProgress.Visible = true;
                this.pbFileProgress.Visible = true;
                this.lFullProgress.Visible = true;
                this.pbFullProgress.Visible = true;
                this.twElementTree.Nodes.Clear();
                Application.DoEvents();

                //=============================================================
                xmlDataDocument = new XmlDocument();
                xmlDataDocument.Load(@"FieldsDesc.xml");
                XmlElement xmlFieldsElement = xmlDataDocument.DocumentElement;
                xmlResultDocument = new XmlDocument();
                xmlResultDocument.AppendChild(xmlResultDocument.CreateXmlDeclaration("1.0", "utf-8", null));
                XmlElement xmlResultObjectsElement = xmlResultDocument.CreateElement("OBJECTS");
                xmlResultDocument.AppendChild(xmlResultObjectsElement);
                //=======================================================================

                foreach (String type in Enum.GetNames(typeof(ItemElements.ObjectTypes)))
                { this.cbTypeFilter.Items.Add(type); }

                string folderPath = this.fbDialog.SelectedPath;
                this.tbWorkingPath.Text = folderPath;
                String[] fileList = Directory.GetFiles(folderPath);
                this.pbFullProgress.Value = 0;
                fileNamesList = new List<string>();
                foreach (String filePath in fileList)
                {
                    this.pbFileProgress.Value = 0;
                    this.pbFullProgress.Increment(100 / fileList.Length + 1);
                    String fileName = filePath.Substring(folderPath.Length + 1);
                    fileNamesList.Add(fileName);
                    this.cbFileFilter.Items.Add(fileName);
                    this.twElementTree.BeginUpdate();
                    this.twElementTree.Nodes.Add(fileName, fileName);
                    StreamReader sr = new StreamReader(filePath);
                    String line = sr.ReadLine().Trim();
                    if (line == fileName.Split('.')[0])
                        if (sr.ReadLine().Trim() == "")
                            if (Enum.IsDefined(typeof(ItemElements.ObjectTypes), (line = sr.ReadLine().Trim().Trim('[', ']').Split(':')[1])))
                            {
                                ItemElements.ObjectTypes objectType = (ItemElements.ObjectTypes)Enum.Parse(typeof(ItemElements.ObjectTypes), line);

                                // XML BLOCK=======================================================
                                //XmlNode xmlTypeNode = xmlObjectsElement.SelectSingleNode("TYPE[@name='" + objectType.ToString() + "']");
                                XmlElement xmlTypeElement = xmlResultDocument.CreateElement("TYPE");
                                xmlTypeElement.SetAttribute("name", objectType.ToString());
                                xmlTypeElement.SetAttribute("file_id", fileName);
                                xmlResultObjectsElement.AppendChild(xmlTypeElement);
                                //=================================================================

                                if (objectType.Equals(ItemElements.ObjectTypes.ITEM))
                                {
                                    String fileText = sr.ReadToEnd();
                                    sr.Dispose();
                                    String[] fileTextArray = fileText.Split('\n');
                                    for (int count = 0; count < fileTextArray.Length; count++)
                                    {
                                        this.pbFileProgress.Value = (count + 1) * 100 / fileTextArray.Length;
                                        String textLine;
                                        if (count < fileTextArray.Length) // Protection for empty line at end file
                                            if ((textLine = fileTextArray[count].Trim()).StartsWith("[" + objectType.ToString()))
                                            {
                                                // Add line multi-field processing==============================================================

                                                ItemElements.ObjectSubTypes objectSubType =
                                                       (ItemElements.ObjectSubTypes)Enum.Parse(typeof(ItemElements.ObjectSubTypes), textLine.Trim('[', ']').Split(':')[0]);
                                                // XML BLOCK=======================================================
                                                XmlElement xmlSubTypeElement = xmlResultDocument.CreateElement("SUBTYPE");
                                                xmlSubTypeElement.SetAttribute("name", objectSubType.ToString());
                                                xmlSubTypeElement.SetAttribute("object_id", textLine.Trim('[', ']').Split(':')[1]);
                                                XmlNodeList xmlSubTypeFieldsNodeList = xmlFieldsElement.SelectNodes("FIELD[USED/" + objectType.ToString() + "/" + objectSubType.ToString() + "]");
                                                for (int i = 0; i < xmlSubTypeFieldsNodeList.Count; i++)
                                                {
                                                    xmlSubTypeElement.AppendChild(xmlResultDocument.ImportNode(xmlSubTypeFieldsNodeList[i], true));
                                                }
                                                xmlTypeElement.AppendChild(xmlSubTypeElement);
                                                //=================================================================
                                                // TMP Added for subtype filter
                                                //if ((objectSubType.Equals(ItemElements.ObjectSubTypes.ITEM_AMMO))
                                                //    || (objectSubType.Equals(ItemElements.ObjectSubTypes.ITEM_ARMOR))
                                                //    || (objectSubType.Equals(ItemElements.ObjectSubTypes.ITEM_FOOD))
                                                //    || (objectSubType.Equals(ItemElements.ObjectSubTypes.ITEM_TOY))
                                                //    || (objectSubType.Equals(ItemElements.ObjectSubTypes.ITEM_INSTRUMENT))
                                                //    || (objectSubType.Equals(ItemElements.ObjectSubTypes.ITEM_SIEGEAMMO))
                                                //    )
                                                {
                                                    // XML Block====================================================
                                                    ItemElements.AddField(textLine, xmlSubTypeElement);
                                                    //================================================================
                                                    this.twElementTree.Nodes[fileName].Nodes.Add(xmlSubTypeElement.Attributes["object_id"].Value, xmlSubTypeElement.Attributes["object_id"].Value);
                                                    while (((!(fileTextArray[count + 1].Trim()).StartsWith("[" + objectType.ToString()))))
                                                    {
                                                        textLine = fileTextArray[++count].Trim();
                                                        if (textLine.StartsWith("["))
                                                        {
                                                            // XML Block====================================================
                                                            ItemElements.AddField(textLine, xmlSubTypeElement);
                                                            //================================================================
                                                            // TODO: Fix tree Key===================================================================
                                                            this.twElementTree.Nodes[fileName].Nodes[xmlSubTypeElement.Attributes["object_id"].Value].Nodes.Add(textLine);
                                                            // Fix for FILE END====================================================================
                                                            if ((count + 2) == fileTextArray.Length)
                                                            {
                                                                // XML Block====================================================
                                                                ItemElements.AddField(fileTextArray[count + 1], xmlSubTypeElement);
                                                                //================================================================

                                                                this.twElementTree.Nodes[fileName].Nodes[xmlSubTypeElement.Attributes["object_id"].Value].Nodes.Add(fileTextArray[count + 1]);
                                                                break;
                                                            }
                                                        }
                                                        else if (textLine.Trim() == "")
                                                        {
                                                            // Skip empty line================================================================================
                                                        }
                                                        else
                                                        {
                                                            count++;
                                                            // This is a Comment===========================================================================
                                                        }
                                                    }
                                                }
                                            }

                                    }
                                }
                            }
                    this.twElementTree.EndUpdate();
                    // UNDONE: Need to separate TreeNodes from TreeView Object for Filtering===================================================
                    this.elementTreeNodes = this.twElementTree.Nodes;
                    //this.twElementTree.CollapseAll();
                }
                // TMP Output==================================================================================================================================
                xmlDataDocument.Save("Data.xml");
                xmlResultDocument.Save("Result.xml");

                this.lFileProgress.Visible = false;
                this.pbFileProgress.Visible = false;
                this.lFullProgress.Visible = false;
                this.pbFullProgress.Visible = false;
                Application.DoEvents();

                if (myErrorString != null)
                    MessageBox.Show(myErrorString);
            }
        }

        public ExTableLayoutPanel GetObjectLayoutTable(XmlNodeList fields, ExTableLayoutPanel tlPanel)
        {
            tlPanel.RowCount = 1;
            for (int i = 0; i < fields.Count; i++)
            {
                tlPanel.RowCount++;
                tlPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                Label lElementName = new Label();
                lElementName.Text = fields[i].Attributes["name"].Value;
                lElementName.Name = fields[i].Attributes["name"].Value;
                if (fields[i].SelectSingleNode("DESCRIPTION") != null)
                    ttDescription.SetToolTip(lElementName, fields[i].SelectSingleNode("DESCRIPTION").InnerText.Trim().Replace("          ", " "));
                lElementName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top
                    | System.Windows.Forms.AnchorStyles.Bottom)
                    | System.Windows.Forms.AnchorStyles.Left)
                    | System.Windows.Forms.AnchorStyles.Right)));
                lElementName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                tlPanel.Controls.Add(lElementName, 0, i);

                if (fields[i].Attributes["class"].Value == "textbox")
                {
                    TextBox tbElementText = new TextBox();
                    tbElementText.Name = fields[i].Attributes["name"].Value;
                    tbElementText.Text = fields[i].SelectSingleNode("VALUE").InnerText;
                    tbElementText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top
                        | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
                    tbElementText.TextChanged += new EventHandler(tbElementText_TextChanged);
                    tlPanel.Controls.Add(tbElementText, 1, i);
                }
                else if (fields[i].Attributes["class"].Value == "checkbox")
                {
                    CheckBox cbElementProperty = new CheckBox();
                    //cbElementProperty.Text = "Sample Text";
                    cbElementProperty.Name = fields[i].Attributes["name"].Value;
                    cbElementProperty.Checked = (fields[i].SelectSingleNode("VALUE").InnerText == "TRUE");
                    tlPanel.Controls.Add(cbElementProperty, 1, i);
                }
                else if (fields[i].Attributes["class"].Value == "combobox")
                {
                    ComboBox cmbElementText = new ComboBox();
                    cmbElementText.Name = fields[i].Attributes["name"].Value;
                    cmbElementText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top
                        | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
                    foreach (XmlNode cmbNode in fields[i].SelectSingleNode("POSIBLEVALUES").ChildNodes)
                    {
                        cmbElementText.Items.Add(cmbNode.InnerText);
                    }
                    String cmbElementValue;
                    if ((cmbElementValue = fields[i].SelectSingleNode("VALUE").InnerText) != "")
                    {
                        cmbElementText.SelectedItem = cmbElementValue;
                    }
                    else
                        cmbElementText.Text = "Empty element...";
                    tlPanel.Controls.Add(cmbElementText, 1, i);
                }
                else if (fields[i].Attributes["class"].Value == "object")
                {
                    //lElementName.Text += "_"+fields[i].Attributes["id"].Value;
                    XmlNodeList objectNodeList = fields[i].SelectNodes("FIELD");
                    ExTableLayoutPanel tlPanelObject = new ExTableLayoutPanel();
                    tlPanelObject.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
                    tlPanelObject.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
                    tlPanelObject.AutoSize = true;
                    tlPanelObject.ColumnCount = 2;
                    tlPanelObject.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
                    tlPanelObject.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
                    //tlPanelObject.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
                    tlPanelObject = GetObjectLayoutTable(objectNodeList, tlPanelObject);
                    tlPanel.Controls.Add(tlPanelObject, 1, i);
                }
                // Add Edit Buttons=======================================================================================
                if ((fields[i].Attributes["class"].Value == "object") && (fields[i].Attributes["quant"].Value == "multi"))
                {
                    FlowLayoutPanel buttonPanel = new FlowLayoutPanel();
                    buttonPanel.Name = "EditPanel";
                    // TODO: Add icons for buttons =====================================================================
                    buttonPanel.FlowDirection = FlowDirection.TopDown;
                    Button addEleemntButton = new Button();
                    addEleemntButton.Name = fields[i].Attributes["name"].Value;
                    addEleemntButton.Text = "ADD";
                    //addEleemntButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
                    addEleemntButton.Click += new EventHandler(addElementButton_Click);
                    buttonPanel.Controls.Add(addEleemntButton);

                    Button removeEleemntButton = new Button();
                    removeEleemntButton.Name = fields[i].Attributes["name"].Value + ":" + fields[i].Attributes["id"].Value;
                    removeEleemntButton.Text = "DEL";
                    //deleteEleemntButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
                    removeEleemntButton.Click += new EventHandler(removeElementButton_Click);
                    buttonPanel.Controls.Add(removeEleemntButton);

                    tlPanel.Controls.Add(buttonPanel, 2, i);
                }
                Application.DoEvents();
            }
            return tlPanel;
        }

        private void removeElementButton_Click(object sender, EventArgs e)
        {
            // TODO: Add EditButton Click Handler===================================================================
            String fieldName = ((Button)sender).Name.Split(':')[0];
            int objectId = Convert.ToInt32(((Button)sender).Name.Split(':')[1]);
            XmlNodeList currentFieldNodes = this.selectedXmlNode.SelectNodes("FIELD[@name='" + fieldName + "']");
            if (currentFieldNodes.Count != 1)
            {
                XmlNode currentFieldNode = this.selectedXmlNode.SelectSingleNode("FIELD[@name='" + fieldName + "' and @id='" + objectId + "']");
                this.selectedXmlNode.RemoveChild(currentFieldNode);
                SendDataToPanel();
                this.statusLabel.Text = "Field " + fieldName + " ( " + objectId + " ) deleted!!!";
            }
        }

        private void addElementButton_Click(object sender, EventArgs e)
        {
            String fieldName = ((Button)sender).Name;
            XmlNodeList currentFieldNodes = this.selectedXmlNode.SelectNodes("FIELD[@name='" + fieldName + "']");
            int fieldCount = currentFieldNodes.Count;
            XmlNode newFieldNode = currentFieldNodes[0].Clone();
            newFieldNode.Attributes["id"].Value = fieldCount.ToString();
            currentFieldNodes[0].ParentNode.InsertAfter(newFieldNode, currentFieldNodes[fieldCount - 1]);
            SendDataToPanel();
            this.statusLabel.Text = "Field " + fieldName + " created!!!";
            //xmlResultDocument.Save("Result_Mod1.xml");
        }

        private void сохранитьToolStripButton_Click(object sender, EventArgs e)
        {
            if (this.selectedNode != null)
            {
                this.UpdateObjectData(this.selectedXmlNode, this.tlPanel);
                this.statusLabel.Text = "Data succesfully saved for '" + selectedNode.Text + "' item.";
            }
        }

        public void UpdateObjectData(XmlNode fieldNode, ExTableLayoutPanel tlPanel)
        {
            int objectId = 0;
            String elementName;
            XmlNode currentField;
            XmlNodeList fieldList;
            for (int i = 0; i < tlPanel.RowCount - 1; i++)
            {
                elementName = ((Label)tlPanel.GetControlFromPosition(0, i)).Name;
                fieldList = fieldNode.SelectNodes("FIELD[@name='" + elementName + "']");
                if (fieldList.Count > 1)
                {
                    currentField = fieldList[objectId];
                    objectId++;
                }
                else
                {
                    currentField = fieldList[0];
                }

                if (currentField.Attributes["class"].Value == "textbox")
                {
                    String elementValue = ((TextBox)tlPanel.GetControlFromPosition(1, i)).Text;
                    currentField.SelectSingleNode("VALUE").InnerText = elementValue;
                }
                else if (currentField.Attributes["class"].Value == "checkbox")
                {
                    Boolean elementValue = ((CheckBox)tlPanel.GetControlFromPosition(1, i)).Checked;
                    currentField.SelectSingleNode("VALUE").InnerText = elementValue.ToString().ToUpper();
                }
                else if (currentField.Attributes["class"].Value == "combobox")
                {
                    String elementValue = (String)((ComboBox)tlPanel.GetControlFromPosition(1, i)).SelectedItem;
                    currentField.SelectSingleNode("VALUE").InnerText = elementValue;
                }
                else if (currentField.Attributes["class"].Value == "object")
                {
                    this.UpdateObjectData(currentField, (ExTableLayoutPanel)tlPanel.GetControlFromPosition(1, i));
                }
            }
            // TMP Output==================================================================================================================================
            xmlResultDocument.Save("Result_Mod.xml");
        }

        private void cbFileFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            // UNDONE: Fix filtering algoritm. Currently not working!!!===============================================================
            TreeNode[] newTreeViewNodes = this.elementTreeNodes.Find(((ToolStripComboBox)sender).SelectedItem.ToString(), false);
            //this.twElementTree.Nodes.Clear();
            //this.twElementTree.Nodes.AddRange(newTreeViewNodes);
        }

        private void создатьToolStripButton_Click(object sender, EventArgs e)
        {
            CreateObjectForm createNewObjectForm = new CreateObjectForm();
            createNewObjectForm.StartPosition = FormStartPosition.CenterParent;
            this.AddOwnedForm(createNewObjectForm);
            this.OwnedForms[0].ShowDialog();
        }

        public void CreateNewObject(String objectType, String objectSubType, String fileName, String objectName)
        {
            this.OwnedForms[0].Close();
            XmlElement newElement = this.xmlResultDocument.CreateElement("SUBTYPE");
            newElement.InnerXml = this.xmlDataDocument.DocumentElement.SelectSingleNode("//SUBTYPE[@name='" + objectSubType + "']").InnerXml;
            newElement.SetAttribute("name", objectSubType);
            newElement.SetAttribute("object_id", objectName);
            newElement.SelectSingleNode("FIELD[@name='" + objectSubType + "']/VALUE").InnerText = objectName;
            this.xmlResultDocument.DocumentElement.SelectSingleNode("//TYPE[@file_id='" + fileName + "']").AppendChild(newElement);

            this.selectedXmlNode = newElement;
            this.selectedNode = this.twElementTree.Nodes[fileName].Nodes.Add(objectName, objectName);
            this.twElementTree.SelectedNode = this.selectedNode;
            this.SendDataToPanel();
            this.statusLabel.Text = "Item seccesfully created.";
            xmlResultDocument.Save("Result_Mod.xml");
        }

        private void twElementTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if ((this.twElementTree.SelectedNode != null) && (this.twElementTree.SelectedNode.Level == 0))
            {
                this.selectedNode = this.twElementTree.SelectedNode;
                if (this.twElementTree.SelectedNode.IsExpanded == false)
                {
                    //this.twElementTree.CollapseAll();
                    this.twElementTree.Nodes[selectedNode.Index].Expand();
                }
                else
                {
                    //this.twElementTree.CollapseAll();
                    this.twElementTree.Nodes[selectedNode.Index].Collapse();
                }
            }
            else if ((this.twElementTree.SelectedNode != null) && (this.twElementTree.SelectedNode.Level == 1))
            {
                this.selectedNode = this.twElementTree.SelectedNode;

                #region If need expand elements in TreeView uncomment this!!!
                //if (this.twElementTree.SelectedNode.IsExpanded == false)
                //{
                //    //this.twElementTree.CollapseAll();
                //    this.twElementTree.Nodes[selectedNode.Parent.Index].Nodes[selectedNode.Index].Expand();
                //}
                //else
                //{
                //    //this.twElementTree.CollapseAll();
                //    this.twElementTree.Nodes[selectedNode.Parent.Index].Nodes[selectedNode.Index].Collapse();
                //}
                #endregion

                this.selectedXmlNode = xmlResultDocument.SelectSingleNode("//SUBTYPE[@object_id='" + selectedNode.Name + "']");
                this.SendDataToPanel();
            }
        }

        private void SendDataToPanel()
        {
            this.tlPanel.Visible = false;
            this.Controls.Remove(fieldsPanel);
            fieldsPanel.Controls.Remove(tlPanel);
            Application.DoEvents();
            tlPanel.Controls.Clear();
            tlPanel.RowStyles.Clear();
            tlPanel = this.GetObjectLayoutTable(this.selectedXmlNode.ChildNodes, tlPanel);
            fieldsPanel.Controls.Add(tlPanel);
            this.Controls.Add(fieldsPanel);
            this.tlPanel.Visible = true;
            Application.DoEvents();
        }

        private void tbElementText_TextChanged(object sender, EventArgs e)
        {
            XmlNode fieldNode = this.selectedXmlNode.SelectSingleNode("//FIELD[@name='" + ((TextBox)sender).Name + "']");
            if (fieldNode != null)
                if (fieldNode.Attributes["type"].Value == "int")
                {
                    int exNumb = 0;
                    if ((((TextBox)sender).Text == "") && (fieldNode.Attributes["essential"].Value == "true"))
                    {
                        ShowError((TextBox)sender, 4);
                        return;
                    }
                    else if ((((TextBox)sender).Text != "") && (!int.TryParse(((TextBox)sender).Text, out exNumb)))
                    {
                        ShowError((TextBox)sender, 3);
                        return;
                    }
                    else if ((fieldNode.Attributes["min"].Value != "") && (int.Parse(fieldNode.Attributes["min"].Value) > exNumb))
                    {
                        //int minNumb = int.Parse(fieldNode.Attributes["min"].Value);
                        ShowError((TextBox)sender, 2);
                        return;
                    }
                    else if ((fieldNode.Attributes["max"].Value != "") && (int.Parse(fieldNode.Attributes["max"].Value) < exNumb))
                    {
                        //int maxNumb = int.Parse(fieldNode.Attributes["max"].Value);
                        ShowError((TextBox)sender, 1);
                        return;
                    }
                    else
                    {
                        ShowError((TextBox)sender, 0);
                        return;
                    }
                }
        }

        private void ShowError(TextBox tbEditor, int errorNumb)
        {
            //myErrorProvider.SetIconAlignment(tbEditor, ErrorIconAlignment.MiddleRight);
            myErrorProvider.SetIconPadding(tbEditor, -20);
            if (errorNumb == 0)
            {
                myErrorProvider.SetError(tbEditor, String.Empty);
                tbEditor.BackColor = Color.White;
                return;
            }
            else if (errorNumb == 1)
            {
                myErrorProvider.SetError(tbEditor, "Value is too large!");
                tbEditor.BackColor = Color.Red;
                return;
            }
            else if (errorNumb == 2)
            {
                myErrorProvider.SetError(tbEditor, "Value is too small!");
                tbEditor.BackColor = Color.Red;
                return;
            }
            else if (errorNumb == 3)
            {
                myErrorProvider.SetError(tbEditor, "Value must be a number!");
                tbEditor.BackColor = Color.Red;
                return;
            }
            else if (errorNumb == 4)
            {
                myErrorProvider.SetError(tbEditor, "Value can't be empty!");
                tbEditor.BackColor = Color.Red;
                return;
            }
        }
    }
}

