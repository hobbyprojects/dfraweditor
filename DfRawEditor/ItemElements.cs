﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

// http://dwarffortresswiki.org/index.php/Category:DF2012:Modding Token Info
namespace DfRawEditor
{
    class ItemElements
    {
        public enum ObjectTypes
        {
            NONE, BODY_DETAIL_PLAN, BODY, BUILDING, CREATURE_VARIATION, CREATURE, DESCRIPTOR, ENTITY,
            INORGANIC, INTERACTION, ITEM, LANGUAGE, MATERIAL_TEMPLATE, PLANT, REACTION, TISSUE_TEMPLATE
        }

        public enum ObjectSubTypes
        {
            NONE, BODY_DETAIL_PLAN, BODY, BODYGLOSS, BUILDING_WORKSHOP, CREATURE_VARIATION, CREATURE, DESCRIPTOR, ENTITY,
            INORGANIC, INTERACTION, ITEM_AMMO, ITEM_ARMOR, ITEM_FOOD, ITEM_GLOVES, ITEM_HELM, ITEM_INSTRUMENT, ITEM_PANTS,
            ITEM_SHIELD, ITEM_SHOES, ITEM_SIEGEAMMO, ITEM_TOOL, ITEM_TOY, ITEM_TRAPCOMP, ITEM_WEAPON, LANGUAGE,
            MATERIAL_TEMPLATE, MAXAGE, PLANT, REACTION, SET_BP_GROUP, TISSUE, TISSUE_TEMPLATE
        }

        public enum ItemAmmoToken
        {
            ITEM_AMMO, NAME, CLASS, SIZE, ATTACK
        }
        public enum ItemArmorToken
        {
            ITEM_ARMOR, NAME, ARMORLEVEL, UBSTEP, LBSTEP, SHAPED, LAYER, COVERAGE, LAYER_SIZE, LAYER_PERMIT, MATERIAL_SIZE,
            SOFT, HARD, LEATHER, METAL, PREPLURAL, MATERIAL_PLACEHOLDER, STRUCTURAL_ELASTICITY_CHAIN_ALL, STRUCTURAL_ELASTICITY_WOVEN_THREAD
        }
        public enum ItemFoodToken
        {
            ITEM_FOOD, NAME, LEVEL
        }
        public enum ItemGlovesToken
        {
            ITEM_GLOVES, NAME, ARMORLEVEL, UPSTEP, SHAPED, LAYER, COVERAGE, LAYER_SIZE, LAYER_PERMIT, MATERIAL_SIZE, SCALED,
            BARRED, LEATHER, METAL, HARD, SOFT, STRUCTURAL_ELASTICITY_CHAIN_ALL, STRUCTURAL_ELASTICITY_WOVEN_THREAD
        }
        public enum ItemHelmToken
        {
            ITEM_HELM, NAME, ARMORLEVEL, UPSTEP, SHAPED, LAYER, COVERAGE, LAYER_SIZE, LAYER_PERMIT, MATERIAL_SIZE, SCALED,
            BARRED, LEATHER, METAL, HARD, SOFT, METAL_ARMOR_LEVELS, STRUCTURAL_ELASTICITY_CHAIN_ALL, STRUCTURAL_ELASTICITY_WOVEN_THREAD
        }
        public enum ItemInstrumentToken
        {
            ITEM_INSTRUMENT, NAME, HARD_MAT
        }
        public enum ItemPantsToken
        {
            ITEM_PANTS, PREPLURAL, NAME, LBSTEP, LAYER, SHAPED, SCALED, COVERAGE, LAYER_SIZE, LAYER_PERMIT, MATERIAL_SIZE, HARD, SOFT, BARRED,
            LEATHER, METAL, CHAIN_METAL_TEXT, METAL_ARMOR_LEVELS, ARMORLEVEL, STRUCTURAL_ELASTICITY_WOVEN_THREAD, STRUCTURAL_ELASTICITY_CHAIN_METAL
        }
        public enum ItemShieldToken
        {
            ITEM_SHIELD, NAME, ARMORLEVEL, BLOCKCHANCE, UPSTEP, MATERIAL_SIZE
        }
        public enum ItemShoesToken
        {
            ITEM_SHOES, PREPLURAL, NAME, UPSTEP, LBSTEP, LAYER, COVERAGE, LAYER_SIZE, LAYER_PERMIT, MATERIAL_SIZE, HARD, SOFT, BARRED,
            LEATHER, METAL, ARMORLEVEL, METAL_ARMOR_LEVELS, STRUCTURAL_ELASTICITY_WOVEN_THREAD, STRUCTURAL_ELASTICITY_CHAIN_METAL
        }
        public enum ItemSiegeAmmoToken
        {
            ITEM_SIEGEAMMO, NAME, CLASS
        }
        public enum ItemToolToken
        {
            ITEM_TOOL, NAME, VALUE, METAL_MAT, SOFT_MAT, HARD_MAT, METAL_WEAPON_MAT, WOOD_MAT, FURNITURE, TOOL_USE, TILE, SIZE,
            MATERIAL_SIZE, CONTAINER_CAPACITY, SKILL, TWO_HANDED, MINIMUM_SIZE, UNIMPROVABLE, INVERTED_TILE, ADJECTIVE, ATTACK
        }
        public enum ItemToyToken
        {
            ITEM_TOY, NAME, HARD_MAT
        }
        public enum ItemTrapCompToken
        {
            ITEM_TRAPCOMP, NAME, IS_SPIKE, ADJECTIVE, SIZE, HITS, MATERIAL_SIZE, IS_SCREW, WOOD, METAL, ATTACK
        }
        public enum ItemWeaponToken
        {
            ITEM_WEAPON, NAME, SIZE, SKILL, TWO_HANDED, RANGED, SHOOT_FORCE, SHOOT_MAXVEL, ADJECTIVE, CAN_STONE, TRAINING, MINIMUM_SIZE, MATERIAL_SIZE, ATTACK
        }





        // UNDONE: TMP Block, Need total rewrite===============================================================
        public enum BodyToken
        {
            BODY, BP, CONTYPE, HEAD, UPPERBODY, LOWERBODY, CATEGORY, STANCE, DEFAULT_RELSIZE, LEFT, RIGHT, LIMB, EMBEDDED,
            SMALL, NUMBER, INTERNAL
        }

        public static void AddField(String fieldText, XmlElement dataElement)
        {
            String[] dataArray = fieldText.Trim().Trim('[', ']').Trim().Split(':');
            String fieldName = dataArray[0];
            XmlNode xmlFieldNode = dataElement.SelectSingleNode("FIELD[@name='" + fieldName + "']");
            if (xmlFieldNode != null)
            {
                ////// Fix for ITEM_FOOD Short Name Token
                if ((dataElement.Attributes["name"].Value == "ITEM_FOOD") && (fieldName == "NAME"))
                {
                    xmlFieldNode.SelectSingleNode("FIELD[@name='NAME']/VALUE").InnerText = dataArray[1];
                    xmlFieldNode.Attributes["used"].Value = "true";
                    return;
                }
                ////// End of Fix

                if (dataArray.Length == 1)
                {
                    xmlFieldNode.SelectSingleNode("VALUE").InnerText = "TRUE";
                }
                else if (dataArray.Length == 2)
                {
                    xmlFieldNode.SelectSingleNode("VALUE").InnerText = dataArray[1];
                }
                else if (dataArray.Length == 3)
                {
                    if (fieldName == "NAME")
                    {
                        xmlFieldNode.SelectSingleNode("FIELD[@name='NAME']/VALUE").InnerText = dataArray[1];
                        xmlFieldNode.SelectSingleNode("FIELD[@name='NAMES']/VALUE").InnerText = dataArray[2];
                    }
                }
                else if (dataArray.Length > 3)
                {
                    // TODO: Send to SubFunction=====================================================================
                    int objectCount = dataElement.SelectNodes("FIELD[@name='" + fieldName + "']").Count;
                    if ((objectCount != 1) || (xmlFieldNode.Attributes["used"].Value != "false"))
                    {
                        XmlNode newXmlFieldNode = xmlFieldNode.Clone();
                        newXmlFieldNode.Attributes["id"].Value = objectCount.ToString();
                        dataElement.AppendChild(newXmlFieldNode);
                        xmlFieldNode = dataElement.SelectSingleNode("FIELD[@name='" + fieldName + "' and @id='" + objectCount + "']");
                    }
                    if (fieldName == "ATTACK")
                    {
                        xmlFieldNode.SelectSingleNode("FIELD[@name='ATTACK_TYPE']/VALUE").InnerText = dataArray[1];
                        xmlFieldNode.SelectSingleNode("FIELD[@name='CONTACT_AREA']/VALUE").InnerText = dataArray[2];
                        xmlFieldNode.SelectSingleNode("FIELD[@name='PENETRATION_SIZE']/VALUE").InnerText = dataArray[3];
                        xmlFieldNode.SelectSingleNode("FIELD[@name='VERB2ND']/VALUE").InnerText = dataArray[4];
                        xmlFieldNode.SelectSingleNode("FIELD[@name='VERB3ND']/VALUE").InnerText = dataArray[5];
                        xmlFieldNode.SelectSingleNode("FIELD[@name='NOUN']/VALUE").InnerText = dataArray[6];
                        xmlFieldNode.SelectSingleNode("FIELD[@name='VELOCITY_MULTIPLIER']/VALUE").InnerText = dataArray[7];
                    }
                }
                xmlFieldNode.Attributes["used"].Value = "true";
            }
            else
                DfRawEditor.dfRawEditor.myErrorString += fieldText + "\n";
        }
    }
}
