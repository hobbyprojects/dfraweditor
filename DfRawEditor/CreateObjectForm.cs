﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DfRawEditor
{
    public partial class CreateObjectForm : Form
    {
        private String selectedType;
        private String selectedSubType;
        private String selectedFileName;
        private String selectedObjectName;
        public CreateObjectForm()
        {
            InitializeComponent();
        }

        private void CreateObjectForm_Load(object sender, EventArgs e)
        {
            foreach (String typeString in Enum.GetNames(typeof(ItemElements.ObjectTypes)))
            {
                this.cbObjectType.Items.Add(typeString);
            }
        }

        private void cbObjectType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.selectedType = this.cbObjectType.SelectedItem.ToString();
            this.cbObjectSubType.Items.Clear();
            foreach (String subTypeString in Enum.GetNames(typeof(ItemElements.ObjectSubTypes)))
            {
                if (subTypeString.StartsWith(selectedType, StringComparison.InvariantCultureIgnoreCase))
                    this.cbObjectSubType.Items.Add(subTypeString);
            }
            if (this.cbObjectSubType.Items.Count != 0)
            {
                this.cbObjectSubType.SelectedIndex = 0;
                this.cbObjectSubType.Enabled = true;
            }
        }

        private void cbObjectSubType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.selectedSubType = this.cbObjectSubType.SelectedItem.ToString();
            this.cbFileName.Items.Clear();
            foreach (String fileName in dfRawEditor.fileNamesList)
            {
                if (fileName.StartsWith(selectedSubType, StringComparison.InvariantCultureIgnoreCase))
                    this.cbFileName.Items.Add(fileName);
            }
            if (this.cbFileName.Items.Count != 0)
            {
                this.cbFileName.SelectedIndex = 0;
                this.cbFileName.Enabled = true;
                this.tbObjectName.Text = this.selectedSubType;
                this.tbObjectName.Enabled = true;
            }
            else
            {
                this.cbFileName.Items.Clear();
                this.cbFileName.Enabled = false;
                this.tbObjectName.Text = "";
                this.tbObjectName.Enabled = false;
            }
        }

        private void cbFileName_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.selectedFileName = this.cbFileName.SelectedItem.ToString();
        }

        private void btCreateObject_Click(object sender, EventArgs e)
        {
            if (tbObjectName.Text != "")
                this.selectedObjectName = tbObjectName.Text;
            ((dfRawEditor)this.Owner).CreateNewObject(this.selectedType, this.selectedSubType, this.selectedFileName, this.selectedObjectName);
        }
    }
}
