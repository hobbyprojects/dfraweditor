﻿namespace DfRawEditor
{
    partial class dfRawEditor
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dfRawEditor));
            this.fbDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.btnChangeFolder = new System.Windows.Forms.Button();
            this.tbWorkingPath = new System.Windows.Forms.TextBox();
            this.twElementTree = new System.Windows.Forms.TreeView();
            this.ssPanel = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lFullProgress = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbFullProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.lFileProgress = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbFileProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.tsPanel = new System.Windows.Forms.ToolStrip();
            this.создатьToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.открытьToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.сохранитьToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.печатьToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.вырезатьToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.копироватьToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.вставкаToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.справкаToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.filterDescLabel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cbFileFilter = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.cbTypeFilter = new System.Windows.Forms.ToolStripComboBox();
            this.fieldsPanel = new System.Windows.Forms.Panel();
            this.tlPanel = new DfRawEditor.ExTableLayoutPanel();
            this.ttDescription = new System.Windows.Forms.ToolTip(this.components);
            this.myErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.ssPanel.SuspendLayout();
            this.tsPanel.SuspendLayout();
            this.fieldsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.myErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // fbDialog
            // 
            this.fbDialog.Description = "Выберите папку с файлами данных...";
            this.fbDialog.SelectedPath = "..\\..\\df_34_11_win\\raw\\objects";
            this.fbDialog.ShowNewFolderButton = false;
            // 
            // btnChangeFolder
            // 
            this.btnChangeFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangeFolder.Location = new System.Drawing.Point(761, 33);
            this.btnChangeFolder.Name = "btnChangeFolder";
            this.btnChangeFolder.Size = new System.Drawing.Size(45, 23);
            this.btnChangeFolder.TabIndex = 0;
            this.btnChangeFolder.Text = "...";
            this.btnChangeFolder.UseVisualStyleBackColor = true;
            this.btnChangeFolder.Click += new System.EventHandler(this.btnChangeFolder_Click);
            // 
            // tbWorkingPath
            // 
            this.tbWorkingPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbWorkingPath.Location = new System.Drawing.Point(277, 35);
            this.tbWorkingPath.Name = "tbWorkingPath";
            this.tbWorkingPath.ReadOnly = true;
            this.tbWorkingPath.Size = new System.Drawing.Size(478, 20);
            this.tbWorkingPath.TabIndex = 1;
            // 
            // twElementTree
            // 
            this.twElementTree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.twElementTree.Location = new System.Drawing.Point(12, 35);
            this.twElementTree.Name = "twElementTree";
            this.twElementTree.ShowNodeToolTips = true;
            this.twElementTree.Size = new System.Drawing.Size(247, 437);
            this.twElementTree.TabIndex = 2;
            this.twElementTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.twElementTree_AfterSelect);
            // 
            // ssPanel
            // 
            this.ssPanel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.lFullProgress,
            this.pbFullProgress,
            this.lFileProgress,
            this.pbFileProgress});
            this.ssPanel.Location = new System.Drawing.Point(0, 486);
            this.ssPanel.Name = "ssPanel";
            this.ssPanel.Size = new System.Drawing.Size(835, 22);
            this.ssPanel.TabIndex = 4;
            this.ssPanel.Text = "ssPanel";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(820, 17);
            this.statusLabel.Spring = true;
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lFullProgress
            // 
            this.lFullProgress.Name = "lFullProgress";
            this.lFullProgress.Size = new System.Drawing.Size(52, 17);
            this.lFullProgress.Text = "Progress";
            this.lFullProgress.Visible = false;
            // 
            // pbFullProgress
            // 
            this.pbFullProgress.Name = "pbFullProgress";
            this.pbFullProgress.Size = new System.Drawing.Size(100, 16);
            this.pbFullProgress.Visible = false;
            // 
            // lFileProgress
            // 
            this.lFileProgress.Name = "lFileProgress";
            this.lFileProgress.Size = new System.Drawing.Size(73, 17);
            this.lFileProgress.Text = "File progress";
            this.lFileProgress.Visible = false;
            // 
            // pbFileProgress
            // 
            this.pbFileProgress.Name = "pbFileProgress";
            this.pbFileProgress.Size = new System.Drawing.Size(300, 16);
            this.pbFileProgress.Visible = false;
            // 
            // tsPanel
            // 
            this.tsPanel.Enabled = false;
            this.tsPanel.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsPanel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьToolStripButton,
            this.открытьToolStripButton,
            this.сохранитьToolStripButton,
            this.печатьToolStripButton,
            this.toolStripSeparator,
            this.вырезатьToolStripButton,
            this.копироватьToolStripButton,
            this.вставкаToolStripButton,
            this.toolStripSeparator2,
            this.справкаToolStripButton,
            this.toolStripSeparator1,
            this.filterDescLabel,
            this.toolStripSeparator3,
            this.cbFileFilter,
            this.toolStripSeparator4,
            this.cbTypeFilter});
            this.tsPanel.Location = new System.Drawing.Point(0, 0);
            this.tsPanel.Name = "tsPanel";
            this.tsPanel.Size = new System.Drawing.Size(835, 25);
            this.tsPanel.Stretch = true;
            this.tsPanel.TabIndex = 5;
            this.tsPanel.Text = "tsPanel";
            // 
            // создатьToolStripButton
            // 
            this.создатьToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.создатьToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("создатьToolStripButton.Image")));
            this.создатьToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.создатьToolStripButton.Name = "создатьToolStripButton";
            this.создатьToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.создатьToolStripButton.Text = "&Создать";
            this.создатьToolStripButton.Click += new System.EventHandler(this.создатьToolStripButton_Click);
            // 
            // открытьToolStripButton
            // 
            this.открытьToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.открытьToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("открытьToolStripButton.Image")));
            this.открытьToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.открытьToolStripButton.Name = "открытьToolStripButton";
            this.открытьToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.открытьToolStripButton.Text = "&Открыть";
            // 
            // сохранитьToolStripButton
            // 
            this.сохранитьToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.сохранитьToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("сохранитьToolStripButton.Image")));
            this.сохранитьToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.сохранитьToolStripButton.Name = "сохранитьToolStripButton";
            this.сохранитьToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.сохранитьToolStripButton.Text = "&Сохранить";
            this.сохранитьToolStripButton.Click += new System.EventHandler(this.сохранитьToolStripButton_Click);
            // 
            // печатьToolStripButton
            // 
            this.печатьToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.печатьToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("печатьToolStripButton.Image")));
            this.печатьToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.печатьToolStripButton.Name = "печатьToolStripButton";
            this.печатьToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.печатьToolStripButton.Text = "&Печать";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // вырезатьToolStripButton
            // 
            this.вырезатьToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.вырезатьToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("вырезатьToolStripButton.Image")));
            this.вырезатьToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.вырезатьToolStripButton.Name = "вырезатьToolStripButton";
            this.вырезатьToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.вырезатьToolStripButton.Text = "В&ырезать";
            // 
            // копироватьToolStripButton
            // 
            this.копироватьToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.копироватьToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("копироватьToolStripButton.Image")));
            this.копироватьToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.копироватьToolStripButton.Name = "копироватьToolStripButton";
            this.копироватьToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.копироватьToolStripButton.Text = "&Копировать";
            // 
            // вставкаToolStripButton
            // 
            this.вставкаToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.вставкаToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("вставкаToolStripButton.Image")));
            this.вставкаToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.вставкаToolStripButton.Name = "вставкаToolStripButton";
            this.вставкаToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.вставкаToolStripButton.Text = "Вст&авка";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // справкаToolStripButton
            // 
            this.справкаToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.справкаToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("справкаToolStripButton.Image")));
            this.справкаToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.справкаToolStripButton.Name = "справкаToolStripButton";
            this.справкаToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.справкаToolStripButton.Text = "Спр&авка";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // filterDescLabel
            // 
            this.filterDescLabel.Name = "filterDescLabel";
            this.filterDescLabel.Size = new System.Drawing.Size(169, 22);
            this.filterDescLabel.Text = "Filter by FileName or ItemType";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // cbFileFilter
            // 
            this.cbFileFilter.Name = "cbFileFilter";
            this.cbFileFilter.Size = new System.Drawing.Size(200, 25);
            this.cbFileFilter.Text = "Select File";
            this.cbFileFilter.SelectedIndexChanged += new System.EventHandler(this.cbFileFilter_SelectedIndexChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // cbTypeFilter
            // 
            this.cbTypeFilter.Name = "cbTypeFilter";
            this.cbTypeFilter.Size = new System.Drawing.Size(200, 25);
            this.cbTypeFilter.Text = "Select Type";
            // 
            // fieldsPanel
            // 
            this.fieldsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fieldsPanel.AutoScroll = true;
            this.fieldsPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.fieldsPanel.Controls.Add(this.tlPanel);
            this.fieldsPanel.Location = new System.Drawing.Point(277, 62);
            this.fieldsPanel.Name = "fieldsPanel";
            this.fieldsPanel.Size = new System.Drawing.Size(546, 410);
            this.fieldsPanel.TabIndex = 0;
            // 
            // tlPanel
            // 
            this.tlPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlPanel.AutoScroll = true;
            this.tlPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tlPanel.ColumnCount = 3;
            this.tlPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 89F));
            this.tlPanel.Location = new System.Drawing.Point(14, 10);
            this.tlPanel.Name = "tlPanel";
            this.tlPanel.RowCount = 1;
            this.tlPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlPanel.Size = new System.Drawing.Size(514, 397);
            this.tlPanel.TabIndex = 0;
            this.tlPanel.Visible = false;
            // 
            // ttDescription
            // 
            this.ttDescription.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.ttDescription.ToolTipTitle = "Description";
            // 
            // myErrorProvider
            // 
            this.myErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink;
            this.myErrorProvider.ContainerControl = this;
            // 
            // dfRawEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 508);
            this.Controls.Add(this.fieldsPanel);
            this.Controls.Add(this.tsPanel);
            this.Controls.Add(this.ssPanel);
            this.Controls.Add(this.twElementTree);
            this.Controls.Add(this.tbWorkingPath);
            this.Controls.Add(this.btnChangeFolder);
            this.Name = "dfRawEditor";
            this.Text = "Dwarf Fortress RAW Editor v0.6 Alpha";
            this.ssPanel.ResumeLayout(false);
            this.ssPanel.PerformLayout();
            this.tsPanel.ResumeLayout(false);
            this.tsPanel.PerformLayout();
            this.fieldsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.myErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog fbDialog;
        private System.Windows.Forms.Button btnChangeFolder;
        private System.Windows.Forms.TextBox tbWorkingPath;
        private System.Windows.Forms.TreeView twElementTree;
        private System.Windows.Forms.StatusStrip ssPanel;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.ToolStripProgressBar pbFullProgress;
        private System.Windows.Forms.ToolStrip tsPanel;
        private System.Windows.Forms.ToolStripButton создатьToolStripButton;
        private System.Windows.Forms.ToolStripButton открытьToolStripButton;
        private System.Windows.Forms.ToolStripButton сохранитьToolStripButton;
        private System.Windows.Forms.ToolStripButton печатьToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton вырезатьToolStripButton;
        private System.Windows.Forms.ToolStripButton копироватьToolStripButton;
        private System.Windows.Forms.ToolStripButton вставкаToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton справкаToolStripButton;
        private System.Windows.Forms.ToolStripStatusLabel lFullProgress;
        private System.Windows.Forms.ToolStripStatusLabel lFileProgress;
        private System.Windows.Forms.ToolStripProgressBar pbFileProgress;
        private System.Windows.Forms.Panel fieldsPanel;
        private System.Windows.Forms.ToolTip ttDescription;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel filterDescLabel;
        private System.Windows.Forms.ToolStripComboBox cbFileFilter;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripComboBox cbTypeFilter;
        private ExTableLayoutPanel tlPanel;
        private System.Windows.Forms.ErrorProvider myErrorProvider;
    }
}

