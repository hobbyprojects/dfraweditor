﻿namespace DfRawEditor
{
    partial class CreateObjectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbObjectType = new System.Windows.Forms.ComboBox();
            this.cbObjectSubType = new System.Windows.Forms.ComboBox();
            this.tbObjectName = new System.Windows.Forms.TextBox();
            this.btCreateObject = new System.Windows.Forms.Button();
            this.cbFileName = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cbObjectType
            // 
            this.cbObjectType.FormattingEnabled = true;
            this.cbObjectType.Location = new System.Drawing.Point(12, 15);
            this.cbObjectType.Name = "cbObjectType";
            this.cbObjectType.Size = new System.Drawing.Size(181, 21);
            this.cbObjectType.TabIndex = 0;
            this.cbObjectType.SelectedIndexChanged += new System.EventHandler(this.cbObjectType_SelectedIndexChanged);
            // 
            // cbObjectSubType
            // 
            this.cbObjectSubType.Enabled = false;
            this.cbObjectSubType.FormattingEnabled = true;
            this.cbObjectSubType.Location = new System.Drawing.Point(199, 15);
            this.cbObjectSubType.Name = "cbObjectSubType";
            this.cbObjectSubType.Size = new System.Drawing.Size(183, 21);
            this.cbObjectSubType.TabIndex = 1;
            this.cbObjectSubType.SelectedIndexChanged += new System.EventHandler(this.cbObjectSubType_SelectedIndexChanged);
            // 
            // tbObjectName
            // 
            this.tbObjectName.Enabled = false;
            this.tbObjectName.Location = new System.Drawing.Point(199, 55);
            this.tbObjectName.Name = "tbObjectName";
            this.tbObjectName.Size = new System.Drawing.Size(286, 20);
            this.tbObjectName.TabIndex = 2;
            // 
            // btCreateObject
            // 
            this.btCreateObject.Location = new System.Drawing.Point(410, 15);
            this.btCreateObject.Name = "btCreateObject";
            this.btCreateObject.Size = new System.Drawing.Size(75, 23);
            this.btCreateObject.TabIndex = 3;
            this.btCreateObject.Text = "Create";
            this.btCreateObject.UseVisualStyleBackColor = true;
            this.btCreateObject.Click += new System.EventHandler(this.btCreateObject_Click);
            // 
            // cbFileName
            // 
            this.cbFileName.Enabled = false;
            this.cbFileName.FormattingEnabled = true;
            this.cbFileName.Location = new System.Drawing.Point(12, 54);
            this.cbFileName.Name = "cbFileName";
            this.cbFileName.Size = new System.Drawing.Size(181, 21);
            this.cbFileName.TabIndex = 4;
            this.cbFileName.SelectedIndexChanged += new System.EventHandler(this.cbFileName_SelectedIndexChanged);
            // 
            // CreateObjectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 87);
            this.Controls.Add(this.cbFileName);
            this.Controls.Add(this.btCreateObject);
            this.Controls.Add(this.tbObjectName);
            this.Controls.Add(this.cbObjectSubType);
            this.Controls.Add(this.cbObjectType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateObjectForm";
            this.ShowInTaskbar = false;
            this.Text = "CreateObjectForm";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.CreateObjectForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbObjectType;
        private System.Windows.Forms.ComboBox cbObjectSubType;
        private System.Windows.Forms.TextBox tbObjectName;
        private System.Windows.Forms.Button btCreateObject;
        private System.Windows.Forms.ComboBox cbFileName;
    }
}